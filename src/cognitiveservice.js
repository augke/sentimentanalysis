import { fetch } from "@forge/api";

const key = process.env.APIKEY; 
const urlPath =
  "https://textanalyticsapiservice.cognitiveservices.azure.com/text/analytics/v2.1/sentiment?showStats=true";
const headers = {
  "Ocp-Apim-Subscription-Key": `${key}`,
  "Content-Type": "application/json",
};

console.log("Key: ", key);

export const getSentimentFromApi = async (text) => {
  let reqObj = {
    language: "en",
    id: "1",
    text: `${text}`,
  };
  let reqBody = {};
  reqBody.documents = [];
  reqBody.documents.push(reqObj);
  let reqBodyJSON = JSON.stringify(reqBody);
  const getSentimentResponse = await fetch(`${urlPath}`, {
    method: "POST",
    headers: { 
        "Ocp-Apim-Subscription-Key": `${key}`,
        "Content-Type": "application/json",
    },
    body: reqBodyJSON,
  });
  if (getSentimentResponse.ok) {
    const jsonResponse = await getSentimentResponse.json();
    console.log(jsonResponse, null, 2);
    return jsonResponse.documents[0].score;
  } else {
    console.log("Error: ", getSentimentResponse.statusText);
    throw new Error();
  }
};
