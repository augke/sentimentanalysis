import ForgeUI, {
  ContentAction,
  ModalDialog,
  useAction,
  useProductContext,
  useState,
  render,
  Fragment,
  Macro,
  Text,
} from "@forge/ui";
import api from "@forge/api";
import { getSentimentFromApi } from "./cognitiveservice"

const getContent = async (contentId) => {
  const response = await api.asApp().requestConfluence(`/rest/api/content/${contentId}?expand=body.atlas_doc_format`)
  if (!response.ok) {
    const err = `Error while getting content with contentId ${contentId}: ${response.status} ${response.statusText}`;
    console.error(err);
    throw new Error(err);
}
const d = await response.json();
console.log("Data: ",d);
return d;
};

const getMood = (score) => {
  return score > 0.7 ? "positive tone"
    : score > 0.5 ? "neutral tone"
    : score < 0.3 ? "negative tone"
    : "error";
};

const getSentiment = (data) => {

  if (!data || !data.body || !data.body.atlas_doc_format || !data.body.atlas_doc_format.value) {
    return "";
}

const { body: { atlas_doc_format: { value } } } = data;
const { content: contentList } = JSON.parse(value);

console.log(JSON.stringify(data, null, 2))
console.log(JSON.stringify(value, null, 2))
let scoreArray = [];
for (const content of contentList) {
  for (const text of content.content) {

    console.log("Text: ", text.text);
    const [score] = useAction(sentimentScore => sentimentScore, async () =>{
      return  await getSentimentFromApi(`${text.text}`);
    })
     
    console.log("Score: ", score);
    scoreArray.push(score);
  }
}

let sumOfScores = scoreArray.reduce((x,y) => x+y);
let lenghtOfScore = scoreArray.length;
let averageScore = sumOfScores / lenghtOfScore;
averageScore = averageScore.toFixed(2);

console.log(`Sum ${sumOfScores}, Length ${lenghtOfScore}, Average ${averageScore}`);;
  return averageScore;
};

const App = () => {
  const [isOpen, setOpen] = useState(true);

  if (!isOpen) {
    return null;
  }

  const { contentId } = useProductContext();

  const [data] = useAction(
    () => null,
    async () => await getContent(contentId)
  );

  const sentimentValue = getSentiment(data);

  return (
    <ModalDialog
    closeButtonText="Close"
      header="Post Sentiment Analysis"
      onClose={() => setOpen(false)}
    >
      <Text>{`General Sentiment Score on this post: ${sentimentValue}`}</Text>
      <Text>{`General Sentiment Tone on this post: ${getMood(sentimentValue)}`}</Text>
    </ModalDialog>
  );
};

export const run = render(
  <ContentAction>
    <App/>
  </ContentAction>
);
